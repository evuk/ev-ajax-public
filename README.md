# <ev-ajax\>

This is a simple web component which makes AJAX requests that are tightly coupled to EValue's APIs. For successfull AJAX requests, this web component requires a valid authenication token from the EValue API portal, resource name (this is appended to the base url `https://api.evalueproduction.com`) & request body to send.

## Proxy Service Endpoint

If however you wish to hide the authentication token from client side inspection, you can provide a endpoint to your own proxy service that can process the API call you wish to make.

## Browser Support
This component has been tested with the following browsers

* **Google Chorme**
* **Firefox**
* **Microsoft Edge**
* **Safari**

## Installation
---
Although the ev-ajax is a bower package, it is not registered in the bower registry. To install it you will need to tell bower CLI where to look for the package. So intead of running

```sh
$ bower install –-save ev-ajax-public
```
you will need to run:

```sh
$ bower install --save https://bitbucket.org/evuk/ev-ajax-public
```
Alternatively, you can create a bower.json by hand

```json
{
    "name": "your-application",
    "dependencies": {
		"ev-ajax-public": "https://bitbucket.org/evuk/ev-ajax-public.git#v1.0.0"
    }
}
```
Then run ```bower install``` in the directory where the bower.json resides.

## Including component in HTML Example
---

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes">
    <title>Demo</title>
    <link rel="import" href="bower_components/ev-ajax-public/ev-ajax.html">
    <script src="bower_components/webcomponentsjs/webcomponents-loader.js" defer></script>
  </head>
  <body>
    <div>
      <h3>Component Demo</h3>
      <ev-ajax></ev-ajax>
    </div>
  </body>
</html>
```

Look at [ev-ajax.html](ev-ajax.html) to learn more about which properties you can set in order
to perform a AJAX request.

## Caveats
---
It's important to note that a proxy service endpoint will **always** take precedence even if a valid resource name and authentication token is defined.

```html
  <ev-ajax
    id="proxy_call"
    authentication-token="000000-000-000-000-000000000"
    resource="state-benefit/1.0.0/stateBenefitAge/age"
    proxy="http://business-api.evalue.newbury/businessJAXWS/api/facade/process">
  </ev-ajax>

  ...

  <script>
    let evAjax = document.querySelector('ev-ajax');
    // proxy endpoint url will be used
    evAjax.generateRequest();
  </script>

```

## Events
---

There are 3 custom events that can be used:

* `success` - Fired when a response is successfully received
* `api-error` - Fired when a error is received from either a proxy server or EValue's API Portal.
* `request` - Fired when a request is sent.


In javscript you can define event listeners like this:

```javascript
let evAjax = document.querySelector('ev-ajax');
evAjax.addEventListener('response', (event) => {
  console.log(event.detail.response);
});

```

Or you can define event listeners in HTML:

```html
  <ev-ajax on-response="handleResponse"></ev-ajax>
```

## Polyfills
---

As HTML Web Components use a set of new standards, these are not natively supported by older browsers and require a set of polyfills so that all features work as expected. The polyfills Javascript library contains a ‘loader’ script which will check the user’s browser to see which of the four main specifications that web components are based on are supported. It will then load the polyfills required for the component to display and function correctly.

There are two main ways to load the polyfills:

**webcomponents-lite.js** includes all of the polyfills necessary to run on any of the supported browsers. Because all browsers receive all polyfills, there is an extra overhead when using this.

**webcomponents-loader.js** performs client-side feature-detection and loads just the required polyfills. This requires an extra round-trip to the server but saves bandwidth for browsers that support one or more features.

Your website will need to include one of the two methods of loading polyfills to ensure the video functions across all supported browsers, for example:

```html
<script src="bower_components/webcomponentsjs/webcomponents-loader.js"></script>
```

Click [here](https://github.com/webcomponents/webcomponentsjs) to learn more about **webcomponentsloader.js**